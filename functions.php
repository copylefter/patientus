<?php
/**
 * patientus functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package patientus
 */

if ( ! function_exists( 'patientus_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function patientus_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on patientus, use a find and replace
		 * to change 'patientus' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'patientus', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => __( 'Primary', 'patientus' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'patientus_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'patientus_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function patientus_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'patientus_content_width', 640 );
}
add_action( 'after_setup_theme', 'patientus_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function patientus_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'patientus' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here.', 'patientus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'patientus_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

 add_action( 'wp_enqueue_scripts', function(){
     if (is_admin()) return; // don't dequeue on the backend
     wp_deregister_script( 'jquery' );
     wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), null, false );
     wp_enqueue_script( 'jquery');
 });

function patientus_scripts() {
	wp_enqueue_style( 'patientus-basic-style', get_stylesheet_uri() );
	// wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700', false );
	wp_enqueue_style( 'font-awesome-5', get_template_directory_uri() . '/css/fontawesome.min.css', 	array(), 	'5.3.0' );
	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/css/slick.css' );
	wp_enqueue_script( 'slick-script', get_template_directory_uri() . '/js/slick.min.js' );



	wp_enqueue_style( 'patientus-main-style', get_template_directory_uri() . '/style/style.css', 	array(), 	'1.0.0' );
	wp_enqueue_script('patientus-script', get_template_directory_uri() . '/js/patientus.js', array(), '20193007', true );

	wp_enqueue_script( 'patientus-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'patientus-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if (is_home()){
		// wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
		// wp_enqueue_script( 'slick-script', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );

	}
}
add_action( 'wp_enqueue_scripts', 'patientus_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


function wpdocs_custom_excerpt_length( $length ) {
    return 65;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


function show_publish_button(){
    Global $post;
    //only print fi admin
    if ((current_user_can('edit_published_posts')) && !is_preview()){
        echo '<form class="hide" name="front_end_publish" method="POST" action="">
                <input type="hidden" name="pid" id="pid" value="'.$post->ID.'">
                <input type="hidden" name="FE_HIDE" id="FE_HIDE" value="FE_HIDE">
                <input type="submit" name="submit" id="submit" value="Hide">
            </form>';
    }
		elseif((current_user_can('manage_options')) && is_preview()){
			echo '<form class="hide" name="front_end_publish" method="POST" action="">
							<input type="hidden" name="pid" id="pid" value="'.$post->ID.'">
							<input type="hidden" name="FE_PUBLISH" id="FE_PUBLISH" value="FE_PUBLISH">
							<input type="submit" name="submit" id="submit" value="Show">
					</form>';
		}
}

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

	if( function_exists('acf_add_options_page') ) {

		$parent = acf_add_options_page(array(
			'page_title' 	=> __('Sliders and header titles', 'patientus' ),
			'menu_title'	=> __('Sliders and titles', 'patientus' ),
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> true,
			'icon_url' 		=> 'dashicons-images-alt2',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> __('Site Settings', 'patientus' ),
			'menu_title'	=> __('Settings', 'patientus' ),
			'parent_slug' => $parent['menu_slug'],
			'menu_slug' 	=> 'acf-options-settings',
		));
		acf_add_options_sub_page(array(
			'page_title' 	=> __('Slider on homepage', 'patientus' ),
			'menu_title'	=> __('Home Slider', 'patientus' ),
			'parent_slug' => $parent['menu_slug'],
			'menu_slug' 	=> 'acf-options-home-slider',
		));
		acf_add_options_sub_page(array(
			'page_title' 	=> __('Doctor slider', 'patientus' ),
			'menu_title'	=> __('Doctor slider', 'patientus' ),
			'parent_slug' => $parent['menu_slug'],
			'menu_slug' 	=> 'acf-options-doctor-slider',
		));

	}
	if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5d43e017782bc',
		'title' => __('Doctor slider', 'patientus' ),
		'fields' => array(
		array(
			'key' => 'field_5d7b85364bde7',
			'label' => __('Display slider with doctors', 'patientus' ),
			'name' => 'doctorslider-display',
			'type' => 'true_false',
			'instructions' => __('Select Off if you want to disable slider with doctors on frontpage.', 'patientus' ),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => __('On', 'patientus' ),
			'ui_off_text' => __('Off', 'patientus' ),
		),
			array(
				'key' => 'field_5d43e035fd7a9',
				'label' => __('doctor slider', 'patientus' ),
				'name' => 'doctor_slider',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array(
					array(
						'key' => 'field_5d43e1ebfd7aa',
						'label' => __('doctor slide', 'patientus' ),
						'name' => 'doctor_slide',
						'type' => 'group',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'layout' => 'block',
						'sub_fields' => array(
							array(
								'key' => 'field_5d43e20dfd7ab',
								'label' => __('Photo', 'patientus' ),
								'name' => 'photo',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
								'preview_size' => 'medium',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
							),
							array(
								'key' => 'field_5d43e261fd7ac',
								'label' => __('Name', 'patientus' ),
								'name' => 'name',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'field_5d43e26afd7ad',
								'label' => __('category', 'patientus' ),
								'name' => 'category',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'field_5d43e359fd7ae',
								'label' => __('review', 'patientus' ),
								'name' => 'review',
								'type' => 'textarea',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'maxlength' => '',
								'rows' => '',
								'new_lines' => '',
							),
						),
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-doctor-slider',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	acf_add_local_field_group(array(
		'key' => 'group_5d4434f74072a',
		'title' => __('Home slider', 'patientus' ),
		'fields' => array(
			array(
				'key' => 'field_5d7b8e737ac73',
				'label' => __('Display main slider', 'patientus' ),
				'name' => 'display_main_slider',
				'type' => 'true_false',
				'instructions' => __('Select Off if you want to disable main slider on frontpage', 'patientus' ),
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 1,
				'ui' => 1,
				'ui_on_text' => __('On', 'patientus' ),
				'ui_off_text' => __('Off', 'patientus' ),
			),
			array(
				'key' => 'field_5d4435079974a',
				'label' => __('Slider title', 'patientus' ),
				'name' => 'slider_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5d4435259974b',
				'label' => __('Slider subtitle', 'patientus' ),
				'name' => 'slider_subtitle',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5d4435339974c',
				'label' => __('Slider container', 'patientus' ),
				'name' => 'slider_container',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array(
					array(
						'key' => 'field_5d44353d9974d',
						'label' => __('Slider', 'patientus' ),
						'name' => 'slider',
						'type' => 'group',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'layout' => 'block',
						'sub_fields' => array(
							array(
								'key' => 'field_5d44354a9974e',
								'label' => __('Slider Image', 'patientus' ),
								'name' => 'slide_image',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
								'preview_size' => 'medium',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
							),
							array(
								'key' => 'field_5d4435999974f',
								'label' => __('Slide Title', 'patientus' ),
								'name' => 'slide_title',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'field_5d4435c699750',
								'label' => __('Slide Description', 'patientus' ),
								'name' => 'slide_description',
								'type' => 'textarea',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'maxlength' => '',
								'rows' => '',
								'new_lines' => '',
							),
						),
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-home-slider',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	acf_add_local_field_group(array(
		'key' => 'group_5d443fd81f8ef',
		'title' => __('Site title', 'patientus' ),
		'fields' => array(
			array(
				'key' => 'field_5d44400a8cca2',
				'label' => __('Site title', 'patientus' ),
				'name' => 'site_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => __('Welcome to dashboard', 'patientus' ),
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5d4440508cca3',
				'label' => __('Site subtitle', 'patientus' ),
				'name' => 'site_subtitle',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-settings',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	acf_add_local_field_group(array(
		'key' => 'group_5d5e9f07a1a73',
		'title' => __('Global feed', 'patientus' ),
		'fields' => array(
			array(
				'key' => 'field_5d5e9f18802d6',
				'label' => __('Hide global feed', 'patientus' ),
				'name' => 'hide_global_feed',
				'type' => 'true_false',
				'instructions' => __('Do you want to hide all news from Patientus?', 'patientus'),
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-settings',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	endif;
}





add_action( 'pre_get_posts', 'wpsites_exclude_translated_posts' );

function wpsites_exclude_translated_posts( $query ) {
$hide = get_field('hide_global_feed', 'option');

	if ($hide){
		if ( $query->is_main_query()  && $query->is_home() ) {
				$meta_query = array(
								 array(
										'key'=>'dt_original_post_id',
										'compare'=>'NOT EXISTS',
								 ),
				);
				$query->set('meta_query',$meta_query);
		}
	}
}


function remove_menus() {
	$user = wp_get_current_user();
	$allowed_roles = array('editor', 'author', 'contributor');
	if( array_intersect($allowed_roles, $user->roles ) ) {
		remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'jetpack' );                    //Jetpack*
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'themes.php' );                 //Appearance
		remove_menu_page( 'plugins.php' );                //Plugins
		remove_menu_page( 'users.php' );                  //Users
		remove_menu_page( 'tools.php' );                  //Tools
		remove_menu_page( 'options-general.php' );        //Settings
	}

}
add_action( 'admin_menu', 'remove_menus' );


add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
	$user = wp_get_current_user();
	$allowed_roles = array('editor', 'author', 'contributor');

	if( array_intersect($allowed_roles, $user->roles ) ) {
		 $wp_admin_bar->remove_node( 'my-sites' );
		 $wp_admin_bar->remove_node( 'wp-logo' );
		 $wp_admin_bar->remove_node( 'comments' );
		 $wp_admin_bar->remove_node( 'distributor' );
		 $wp_admin_bar->remove_node( 'new-media' );
	}
}

add_action('admin_head', 'mytheme_remove_help_tabs');
function mytheme_remove_help_tabs() {
	$user = wp_get_current_user();
	$allowed_roles = array('editor', 'author', 'contributor');
		if( array_intersect($allowed_roles, $user->roles ) ) {
	    $screen = get_current_screen();
	    $screen->remove_help_tabs();
			add_filter('screen_options_show_screen', '__return_false');
		}
}
//remove comments from pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
remove_post_type_support( 'page', 'comments' );
}

function wpdocs_unregister_tags_for_posts() {
 unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'wpdocs_unregister_tags_for_posts' );

function wpdocs_unregister_categories_for_posts() {
 unregister_taxonomy_for_object_type( 'category', 'post' );
}
add_action( 'init', 'wpdocs_unregister_categories_for_posts' );

/**
 * Обрезка текста (excerpt). Шоткоды вырезаются. Минимальное значение maxchar может быть 22.
 *
 * @param string/array $args Параметры.
 *
 * @return string HTML
 *
 * @ver 2.6.4
 */
function kama_excerpt( $args = '' ){
	global $post;

	if( is_string($args) )
		parse_str( $args, $args );

	$rg = (object) array_merge( array(
		'maxchar'   => 360,   // Макс. количество символов.
		'text'      => '',    // Какой текст обрезать (по умолчанию post_excerpt, если нет post_content.
							  // Если в тексте есть `<!--more-->`, то `maxchar` игнорируется и берется все до <!--more--> вместе с HTML.
		'autop'     => true,  // Заменить переносы строк на <p> и <br> или нет?
		'save_tags' => '<strong><b><a>',    // Теги, которые нужно оставить в тексте, например '<strong><b><a>'.
		'more_text' => '...', // Текст ссылки `Читать дальше`.
	), $args );

	$rg = apply_filters( 'kama_excerpt_args', $rg );

	if( ! $rg->text )
		$rg->text = $post->post_excerpt ?: $post->post_content;

	$text = $rg->text;
	$text = preg_replace( '~\[([a-z0-9_-]+)[^\]]*\](?!\().*?\[/\1\]~is', '', $text ); // убираем блочные шорткоды: [foo]some data[/foo]. Учитывает markdown
	$text = preg_replace( '~\[/?[^\]]*\](?!\()~', '', $text ); // убираем шоткоды: [singlepic id=3]. Учитывает markdown
	$text = trim( $text );

	// <!--more-->
	if( strpos( $text, '<!--more-->') ){
		preg_match('/(.*)<!--more-->/s', $text, $mm );

		$text = trim( $mm[1] );

		$text_append = ' <a href="'. get_permalink( $post ) .'#more-'. $post->ID .'">'. $rg->more_text .'</a>';
	}
	// text, excerpt, content
	else {
		$text = trim( strip_tags($text, $rg->save_tags) );

		// Обрезаем
		if( mb_strlen($text) > $rg->maxchar ){
			$text = mb_substr( $text, 0, $rg->maxchar );
			$text = preg_replace( '~(.*)\s[^\s]*$~s', '\\1...', $text ); // убираем последнее слово, оно 99% неполное
		}
	}

	// Сохраняем переносы строк. Упрощенный аналог wpautop()
	if( $rg->autop ){
		$text = preg_replace(
			array("/\r/", "/\n{2,}/", "/\n/",   '~</p><br ?/?>~'),
			array('',     '</p><p>',  '<br />', '</p>'),
			$text
		);
	}

	$text = apply_filters( 'kama_excerpt', $text, $rg );

	if( isset($text_append) )
		$text .= $text_append;

	return ( $rg->autop && $text ) ? "<p>$text</p>" : $text;
}

//disable gravatar link
function space_callback( $example ) {
$example = ''; // Maybe modify more else $example='Hi'
return $example; }
add_filter( 'user_profile_picture_description', 'space_callback' );

//function to check if the current page is a post edit page
function is_edit_page($new_edit = null){
    global $pagenow;
    //make sure we are on the backend
    if (!is_admin()) return false;


    if($new_edit == "edit")
        return in_array( $pagenow, array( 'post.php',  ) );
    elseif($new_edit == "new") //check for new post page
        return in_array( $pagenow, array( 'post-new.php' ) );
    else //check for either new or edit
        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
}


function draft_enqueue_script() {
	if (is_edit_page() || is_singular()){
		 wp_enqueue_script('admin_script', get_template_directory_uri() . '/js/admin.js');
	}
}
add_action( 'wp_enqueue_scripts', 'draft_enqueue_script' );
add_action( 'admin_enqueue_scripts', 'draft_enqueue_script' );

/**
 * When pushing posts, get original date or don't change the destination post date.
 */
function filter_dt_push_post( $post_body, $post) {


		$original_date = $post->post_date;
		$post_body['post_date'] = $original_date;

	return $post_body;
	return $post;

}
add_filter( 'dt_push_post_args', __NAMESPACE__ . '\filter_dt_push_post', 10, 2 );
