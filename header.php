<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package patientus
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'patientus' ); ?></a>

	<header id="masthead" class="site-header ui container">
		<?php if(get_header_image()):?>
			<img id="header-img" src="<?php echo( get_header_image() ); ?>" alt="<?php echo( get_bloginfo( 'title' ) ); ?>" />
			<div class="site-branding has-image">
		<?php else :?>
			<div class="site-branding">
		<?php endif ?>

				<h1 class="site-title"><?php the_field('site_title', 'option') ?></h1>
				<?php if(get_field('site_subtitle', 'option')):?>
					<p class="site-description"><?php the_field( 'site_subtitle', 'option' ); ?></p>
				<?php endif?>


		</div><!-- .site-branding -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
