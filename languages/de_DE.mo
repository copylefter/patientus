��    )      d  ;   �      �     �     �     �  ,   �               "     3     ?  _   K     �     �     �  %   �     �     �     �     �  :   	  C   D     �     �     �     �  
   �     �     �     �     �     �               &     6     C     ]     p     y     �     �  +  �     �     �     �  4   
     ?  	   K     U     i     �  g   �     	     	     	  $   	     7	     ?	     E	     M	  H   e	  G   �	     �	     
     
     &
     4
     @
     T
     k
     {
     �
  
   �
     �
     �
     �
     �
     �
  	   	               %         $              '                                                                  !      %   )         (                 	                 &   
                    #   "                                Add widgets here. Display main slider Display slider with doctors Do you want to hide all news from Patientus? Doctor slider Global feed Hide global feed Home Slider Home slider It looks like nothing was found at this location. Maybe try one of the links below or a search? Name Off On Oops! That page can&rsquo;t be found. Pages: Photo Primary Search Results for: %s Select Off if you want to disable main slider on frontpage Select Off if you want to disable slider with doctors on frontpage. Settings Sidebar Site Settings Site subtitle Site title Skip to content Slide Description Slide Title Slider Slider Image Slider container Slider on homepage Slider subtitle Slider title Sliders and header titles Sliders and titles category doctor slide doctor slider review Project-Id-Version: patientus
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-01-07 13:29+0400
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.4
Last-Translator: 
Language: de
 Widgets hier hinzufügen. Hauptslider anzeigen Zweitslider anzigen Möchten Sie alle Beiträge von Patientus verbergen? Zweitslider Hauptfeed Hauptfeed verbergen Hauptslider auf Dashboard Hauptslider auf Dashboard Es sieht so aus, als wäre hier nichts. Vielleicht einen der folgenden Links oder eine Suche probieren? Titel Aus An Oje! Die Seite wurde nicht gefunden. Seiten: Image Primär Suchergebnisse für: %s  Wählen Sie “Aus”, wenn Sie den Hauptslider nicht anzeigen möchten Wählen Sie “Aus”, wenn Sie den Zweitslider nicht anzeigen möchten Einstellungen Seitenleiste Seiten Einstellungen Site subtitle Seitentitel Zum Inhalt springen Beschreibung der Slide Titel der Slide Slider Bild der Slide Slider Box Hauptslider auf Dashboard Untertitel des Sliders Titel des Sliders Seiten Einstellungen Seiten Einstellungen Kategorie Slide Zweitslider Beschreibung 