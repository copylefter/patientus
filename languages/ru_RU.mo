��    '      T  5   �      `     a     s     �  ,   �     �     �     �     �       _        s     x     |  %        �     �  :   �  C   �     2     ;     C     Q  
   _     j     |     �     �     �     �     �     �     �     �     
          (     5     C  t  J  )   �  2   �  9     Y   V  $   �  D   �  Q   	  "   l	  "   �	  �   �	     `
     g
     p
  4   w
     �
     �
  �   �
  �   N     �     �       #   *     N  !   j  !   �     �  '   �  #   �  "   	  )   ,  !   V  6   x     �  D   �       $   !  $   F     k           	      "   %                   &   !              
                       $                             #                                                           '          Add widgets here. Display main slider Display slider with doctors Do you want to hide all news from Patientus? Doctor slider Global feed Hide global feed Home Slider Home slider It looks like nothing was found at this location. Maybe try one of the links below or a search? Name Off On Oops! That page can&rsquo;t be found. Photo Primary Select Off if you want to disable main slider on frontpage Select Off if you want to disable slider with doctors on frontpage. Settings Sidebar Site Settings Site subtitle Site title Slide Description Slide Title Slider Slider Image Slider container Slider on homepage Slider subtitle Slider title Sliders and header titles Sliders and titles Welcome to dashboard category doctor slide doctor slider review Project-Id-Version: patientus
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-01-07 13:17+0400
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 2.2.4
Last-Translator: 
Language: ru
 Добавьте виджеты сюда. Показывать главный слайдер Показывать слайдер с докторами Хотите ли вы скрыть все новости с главного сайта? Слайдер с докторами Трансляция новостей с главного сайта Скрыть трансляцию новостей с главного сайта Слайдер на главной Слайдер на главной Похоже, в этом месте ничего не было найдено. Возможно, попробуйте одну из ссылок ниже или поиск? Имя ВЫКЛ ВКЛ Упс! Эта страница не найдена. Фото Главное Выберите Выкл., если вы хотите отключить первый слайдер на первой странице. Выберите Выкл., если вы хотите отключить слайдер с врачами на первой странице. Настройки Боковая панель Настройки сайта Подзаголовок сайта Название сайта Описание слайдера Название слайдера Слайдер Изображение слайдера Контейнер слайдера Слайдер на главной Подзаголовок слайдера Название слайдера Слайдеры и заголовки в хэдере Настройки сайта Добро пожаловать в Панель управления Позиция Слайдер с докторами Слайдер с докторами Описание 