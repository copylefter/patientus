<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */

?>
<div class="article-container ui container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if ( has_post_thumbnail() ){ echo 'style="background-image: url(' . get_the_post_thumbnail_url($post->ID, 'full') . ');"';  }	?>>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="close-button"><i class="fas fa-times"></i></a>
		<div class="postblock">
			<header class="entry-header">

			<?php if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta">
						<?php
						patientus_posted_on();
						patientus_entry_footer();
						?>
					</div><!-- .entry-meta -->
				<?php endif; ?>
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
				?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
				the_content( sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'patientus' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				) );

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'patientus' ),
					'after'  => '</div>',
				) );
				?>
			</div><!-- .entry-content -->
		</div>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
