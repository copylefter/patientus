<?php
/**
 * Template part for displaying homepage slider in homepage.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */

?>
<?php if( get_field('display_main_slider', 'option') ): ?>
  <?php if( have_rows('slider_container', 'option') ): ?>
  <div class="slider-block">
    <div class="container ui">
      <div class="slider-title">
        <h2><?php the_field('slider_title', 'option');?></h2>
        <p><?php the_field('slider_subtitle', 'option');?> </p>
      </div>
      <div class="slider-container">
        <div class="sliderhome">
            <?php while( have_rows('slider_container', 'option') ): the_row(); ?>
            <div class="homeslide">
              <?php   $slider = get_sub_field('slider', 'option');
                  if( $slider ): ?>
                  <div class="slide-container">
                    <div class="homeslie-img-container">
                      <img class="homeslide-img" src="<?php echo $slider['slide_image']['url']; ?>" alt="<?php echo $slider['slide_image']['alt']; ?>" />
                    </div>
                		<div class="homeslide-content">
                      <h3 class="homeslide-title"><?php echo $slider['slide_title'];?> </h3>
                			<p><?php echo $slider['slide_description']; ?></p>
                		</div>
                  </div>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>
<?php endif; ?>
