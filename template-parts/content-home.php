<?php
/**
 * Template part for displaying homepage content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */

?>

<div class="article-container ui container">
	<?php
	?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if ( has_post_thumbnail() ){ echo 'style="background-image: url(' . get_the_post_thumbnail_url($post->ID, 'full') . ');"';  }	?>>
		<a class="linkwrapper
			<?php
			 	if ( (!is_user_logged_in()) ) {
					echo  "modal-link";
				};?>"


			href=" <?php echo get_permalink();?>"></a>
		<div class="postblock">
			<header class="entry-header">


			<?php if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta">
						<?php
						patientus_posted_on();
						patientus_entry_footer();
						?>
					</div><!-- .entry-meta -->
				<?php endif; ?>

				<?php
					the_title( '<h2 class="entry-title"><a class="modal-link" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					 echo kama_excerpt();
				?>
			</div><!-- .entry-content -->
		</div>

	</article><!-- #post-<?php the_ID(); ?> -->

</div>
