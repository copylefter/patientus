<?php
/**
 * Template part for displaying doctor slider in homepage.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */

?>
<?php if( get_field('doctorslider-display', 'option') ): ?>
  <?php if( have_rows('doctor_slider', 'option') ): ?>
    <div class="doctorslider-block">
      <div class="container ui">
        <div class="doctorslider-container">
            <div class="doctorslider">

              <?php while( have_rows('doctor_slider', 'option') ): the_row(); ?>
              <div class="doctorslide">
                  <?php
                  $doctor_slide = get_sub_field('doctor_slide', 'option');
                  if( $doctor_slide ): ?>
                  	<div class="slider-top">
                  		<img class="face" src="<?php echo $doctor_slide['photo']['url']; ?>" />
                      <h4 class="name"><?php echo $doctor_slide['name']; ?></h4>
                      <p class="spec"><?php echo $doctor_slide['category']; ?></p>
                    </div>
                    <p class="text"><?php echo $doctor_slide['review']; ?></p>
                  <?php endif; ?>
              </div>
              <?php endwhile; ?>

          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
<?php endif; ?>
