<?php
/* Template Name: Options */
/**
 * The template for adding Options
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */
acf_form_head();
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main ui container">

		<?php
			the_title( '<h1 class="entry-title">', '</h1>' );

			acf_form(array(
				'post_id'		=> 'options',
				'field_groups' => array('group_5d443fd81f8ef','group_5d4434f74072a','group_5d43e017782bc'),
				'submit_value'	=> 'Update the post!'
				));
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
