<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

	<?php
	// if( 0 == $wp_query->found_posts ):
	// 	 get_template_part( 'template-parts/homeslider' );
	// 	 get_template_part( 'template-parts/doctorslider' );
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) :
				// if (1 == $wp_query->found_posts):
					the_post();

					get_template_part( 'template-parts/content', 'home' );

					if (1 == $wp_query->found_posts):
						get_template_part( 'template-parts/homeslider' );
						get_template_part( 'template-parts/doctorslider' );
					elseif (2 == $wp_query->found_posts):
						if( 0 == $wp_query->current_post ):
						 get_template_part( 'template-parts/homeslider' );
						elseif( 1 == $wp_query->current_post ):
						 get_template_part( 'template-parts/doctorslider' );
						endif;
					elseif (3 == $wp_query->found_posts):
						if( 1 == $wp_query->current_post ):
						 get_template_part( 'template-parts/homeslider' );
						elseif( 2 == $wp_query->current_post ):
						 get_template_part( 'template-parts/doctorslider' );
						endif;
					else:
						if( 1 == $wp_query->current_post ):
						 get_template_part( 'template-parts/homeslider' );
						elseif( 3 == $wp_query->current_post ):
						 get_template_part( 'template-parts/doctorslider' );
						endif;
					endif;
				// 	get_template_part( 'template-parts/homeslider' );
				// 	get_template_part( 'template-parts/doctorslider' );
				// elseif (2 == $wp_query->found_posts):
				// 	the_post();
				// 	if( 1 == $wp_query->current_post ):
				// 	 get_template_part( 'template-parts/homeslider' );
				// 	elseif( 2 == $wp_query->current_post ):
				// 	 get_template_part( 'template-parts/doctorslider' );
			 	// 	endif;
				// else:
				// 	the_post();
				// endif;
			endwhile;

			the_posts_navigation();

		else :

		get_template_part( 'template-parts/homeslider' );
		get_template_part( 'template-parts/doctorslider' );

	endif;
	?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
