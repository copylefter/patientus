<?php
/* Template Name: Add Post Page */
/**
 * The template for adding post
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientus
 */
acf_form_head();
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main ui container">

		<?php

			acf_form(array(
				'post_id'		=> 'new_post',
				'field_groups' => array('group_5d51615f742f1'),
				'post_title'	=> true,
				'post_content'	=> true,
				'new_post'		=> array(
					'post_type'		=> 'post',
					'post_status'	=> 'publish',
				),
				'return'		=> home_url(''),
				));
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
