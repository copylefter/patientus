jQuery(document).ready(function($){
  $('.sliderhome').slick({
    dots: true,
    adaptiveHeight: true,
    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left' aria-hidden='true'></i></button>",
    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right' aria-hidden='true'></i></button>",
    responsive: [
    {
      breakpoint: 968,
      settings: {
        arrows: false,
      }
    }
    ]
  });
  $('.doctorslider').slick({
    dots: true,
    adaptiveHeight: true,
    fade: true,
    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left' aria-hidden='true'></i></button>",
    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right' aria-hidden='true'></i></button>",
    responsive: [
    {
      breakpoint: 968,
      settings: {
        arrows: false,
      }
    }
    ]
  });

  $( "#showpost" ).click(function() {
    $( "#acf-form" ).toggle( "slow" );
  });
});
